package com.emiratesauctiontask.task.Data.API;


import com.emiratesauctiontask.task.Data.API.Models.CarsListResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET("carsonline")
    Call<CarsListResponse> getCarsList();
}
