package com.emiratesauctiontask.task.Presentation.CarsListScreen;

import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyHolder;
import com.airbnb.epoxy.EpoxyModelClass;
import com.airbnb.epoxy.EpoxyModelWithHolder;
import com.emiratesauction.task.R;
import com.emiratesauctiontask.task.Data.API.Models.Car;
import com.emiratesauctiontask.task.Utils.LocalHelper;
import com.squareup.picasso.Picasso;

@EpoxyModelClass(layout = R.layout.car_list_cell_custom_view)
public class CarsCellEpoxyModel extends EpoxyModelWithHolder<CarsCellEpoxyModel.Holder> {

    @EpoxyAttribute
    public Car car;

    @EpoxyAttribute
    public Long settingTime;

    private static String IMAGE_WIDTH = "200";
    private static String IMAGE_HIGHT = "200";

    @Override
    protected CarsCellEpoxyModel.Holder createNewHolder() {
        return new CarsCellEpoxyModel.Holder();
    }

    @Override
    public void bind(@NonNull final CarsCellEpoxyModel.Holder holder) {
        //Set car image.
        if (car.getImage() != null && !car.getImage().isEmpty()) {
            String carImage = car.getImage().replace("[w]", IMAGE_WIDTH).replace("[h]", IMAGE_HIGHT);
            Picasso.get().load(carImage).into(holder.carImage);
        }

        //Set model name.
        String carName = LocalHelper.isRTL() ? car.getModelAr() : car.getModelEn();
        holder.carName.setText(carName);


        //set current price.
        String currentPrice = car.getAuctionInfo().getCurrentPrice().toString();
        String currency = LocalHelper.isRTL() ? car.getAuctionInfo().getCurrencyAr() : car.getAuctionInfo().getCurrencyEn();
        String fullPrice = holder.carPrice.getContext().getResources().getString(R.string.car_price, currentPrice, currency);
        holder.carPrice.setText(fullPrice);


        //Set Lot
        holder.lotNumber.setText(String.valueOf(car.getAuctionInfo().getLot()));

        //Set Bids
        holder.bidsNumber.setText(String.valueOf(car.getAuctionInfo().getBids()));

        //Set Time left
        setTimeLeftAndColor(holder, getEndingTime(car.getAuctionInfo().getEndDate()));

        if (holder.countDownTimer != null)
            holder.countDownTimer.cancel();
        holder.countDownTimer = new CountDownTimer(getEndingTime(car.getAuctionInfo().getEndDate()), 1000) {

            public void onTick(long millisUntilFinished) {
                setTimeLeftAndColor(holder, millisUntilFinished);
            }

            public void onFinish() {
                holder.timeLeft.setText(holder.carPrice.getContext().getResources().getString(R.string.end_time,
                        "0", "0", "0"));
            }
        }.start();
    }

    private long getEndingTime(long serverEndingTime) {
        long threshold = System.currentTimeMillis() - settingTime;
        return serverEndingTime - threshold < 0 ? 0 : serverEndingTime - threshold;
    }

    private void setTimeLeftAndColor(@NonNull Holder holder, long timeLeft) {

        int seconds = (int) (timeLeft / 1000) % 60;
        int minutes = (int) ((timeLeft / (1000 * 60)) % 60);
        int hours = (int) ((timeLeft / (1000 * 60 * 60)) % 24);

        String timeLeftString = holder.carPrice.getContext().getResources().getString(R.string.end_time,
                String.valueOf(hours),
                String.valueOf(minutes),
                String.valueOf(seconds));

        holder.timeLeft.setText(timeLeftString);

        if (hours == 0 && minutes < 5) {
            holder.timeLeft.setTextColor(ContextCompat.getColor(holder.timeLeft.getContext(), R.color.colorPrimaryDark));
        } else {
            holder.timeLeft.setTextColor(ContextCompat.getColor(holder.timeLeft.getContext(), R.color.textPrimary));
        }
    }

    @Override
    protected int getDefaultLayout() {
        return R.layout.car_list_cell_custom_view;
    }


    static class Holder extends EpoxyHolder {
        ImageView carImage;
        TextView carName;
        TextView carPrice;
        TextView lotNumber;
        TextView bidsNumber;
        TextView timeLeft;
        private CountDownTimer countDownTimer;

        @Override
        protected void bindView(View itemView) {
            carImage = itemView.findViewById(R.id.car_image_view);
            carName = itemView.findViewById(R.id.car_name_text_view);
            carPrice = itemView.findViewById(R.id.car_price_text_view);
            lotNumber = itemView.findViewById(R.id.lot_text_view);
            bidsNumber = itemView.findViewById(R.id.bids_text_view);
            timeLeft = itemView.findViewById(R.id.time_left_text_view);
        }
    }

}
