package com.emiratesauctiontask.task.Presentation.CarsListScreen;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.BoringLayout;
import android.util.Log;


import com.emiratesauctiontask.task.Data.API.Models.Car;
import com.emiratesauctiontask.task.Data.API.Models.CarsListResponse;
import com.emiratesauctiontask.task.Domain.CarsUseCases.GetCarsUseCase;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarsListViewModel extends ViewModel {

    private MutableLiveData<List<Car>> carsLiveData;
    private MutableLiveData<Boolean> loadingLiveData;
    private GetCarsUseCase getCarsUseCase = new GetCarsUseCase();
    private CountDownTimer countDownTimer;
    private Handler refreshListHandler;

    public MutableLiveData<List<Car>> getCarsListLiveData() {

        if (carsLiveData == null) {
            carsLiveData = new MutableLiveData<List<Car>>();
        }
        return carsLiveData;
    }

    public MutableLiveData<Boolean> getLoadingLiveData() {
        if (loadingLiveData == null) {
            loadingLiveData = new MutableLiveData<Boolean>();
        }
        return loadingLiveData;
    }

    public void init() {
        getCarsList();
    }


    public void refreshCarsList() {
        getCarsList();
    }


    private void getCarsList() {
        loadingLiveData.postValue(true);
        getCarsUseCase.getCarsList(new Callback<CarsListResponse>() {
            @Override
            public void onResponse(Call<CarsListResponse> call, Response<CarsListResponse> response) {
                loadingLiveData.postValue(false);

                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        carsLiveData.postValue(response.body().getCars());
                        autoUpdateList(Long.valueOf(response.body().getTicks()) * 1000);
                    }
                }
            }

            @Override
            public void onFailure(Call<CarsListResponse> call, Throwable t) {
                loadingLiveData.postValue(false);
            }
        });
    }

    private void autoUpdateList(long updateAfterInMills) {
        if (refreshListHandler != null)
            refreshListHandler.removeCallbacksAndMessages(null);

        refreshListHandler = new Handler();
        refreshListHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                getCarsList();
            }
        }, updateAfterInMills);
    }
}
