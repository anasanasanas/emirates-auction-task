package com.emiratesauctiontask.task.Presentation.CarsListScreen;

import com.airbnb.epoxy.EpoxyController;
import com.emiratesauctiontask.task.Data.API.Models.Car;

import java.util.Collections;
import java.util.List;

public class CarsListController extends EpoxyController {

    private List<Car> cars = Collections.emptyList();
    private long settingTime;


    @Override
    protected void buildModels() {
        for (Car car : cars) {
            new CarsCellEpoxyModel_()
                    .id(car.getCarID())
                    .settingTime(settingTime)
                    .car(car)
                    .addTo(this);
        }
    }

    public void setCars(List<Car> cars, long settingTime) {
        this.cars = cars;
        this.settingTime = settingTime;
        requestModelBuild();
    }
}
