package com.emiratesauctiontask.task.Presentation.CarsListScreen;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.emiratesauction.task.R;
import com.emiratesauctiontask.task.Data.API.Models.Car;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarsListActivity extends AppCompatActivity {

    @BindView(R.id.swipeRefreshView)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.my_recycler_view)
    RecyclerView recyclerView;

    private CarsListViewModel carsListViewModel;
    private CarsListController controller;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init butter knife
        ButterKnife.bind(this);

        // Get the ViewModel.
        carsListViewModel = ViewModelProviders.of(this).get(CarsListViewModel.class);

        setLiveDataObservers();

        initCarsListRecyclerView();

        carsListViewModel.init();

        initSwipeToRefreshLayout();
    }


    private void setRefresh(Boolean isRefreshing) {
        if (isRefreshing != null)
            swipeRefreshLayout.setRefreshing(isRefreshing);
    }


    private void setCarsListData(List<Car> cars) {
        controller.setCars(cars, System.currentTimeMillis());
    }

    private void setLiveDataObservers() {
        final Observer<List<Car>> carsLiveDataObserver = new Observer<List<Car>>() {
            @Override
            public void onChanged(@Nullable List<Car> cars) {
                setCarsListData(cars);
            }
        };
        carsListViewModel.getCarsListLiveData().observe(this, carsLiveDataObserver);


        final Observer<Boolean> loadingLiveDataObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isLoading) {
                setRefresh(isLoading);
            }
        };
        carsListViewModel.getLoadingLiveData().observe(this, loadingLiveDataObserver);
    }

    private void initCarsListRecyclerView() {

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        controller = new CarsListController();
        recyclerView.setAdapter(controller.getAdapter());
    }

    private void initSwipeToRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        carsListViewModel.refreshCarsList();
                    }
                }
        );
    }

}
