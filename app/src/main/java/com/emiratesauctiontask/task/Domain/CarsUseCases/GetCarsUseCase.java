package com.emiratesauctiontask.task.Domain.CarsUseCases;


import com.emiratesauctiontask.task.Data.API.ApiInterface;
import com.emiratesauctiontask.task.Data.API.Models.CarsListResponse;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GetCarsUseCase {

    public void getCarsList(Callback<CarsListResponse> carsListResponseCallback) {
        provideRetrofitInstance().getCarsList().enqueue(carsListResponseCallback);
    }


    //TODO: provide Retrofit client with dependence injection.
    private ApiInterface provideRetrofitInstance() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.emiratesauction.com/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ApiInterface.class);
    }
}
